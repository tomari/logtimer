# Logging timer

* 2012-2022 H.Tomari. Public Domain.

## What is this?

![Screenshot](logtimer.png)

This is a tool to aid practicing presentations.

* Hit Start button, and jot down whatever happened in the presentation, such as page flip.
* The event is logged with the timestamp in the text field.
* Send the log to the presenter after the presentation is finished.
* Adjust the timings and add/remove slides so that the content of the presentation is well balanced.

## Requirements

* Tested on python 3.10.4 on Ubuntu 22.04
* requires PyGObject GTK+-3 bindings

